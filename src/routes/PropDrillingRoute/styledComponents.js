import styled from '@emotion/styled'
import tw from 'tailwind.macro'

export const ThemedText = styled.span`
  color: ${props => (props.selectedTheme === 'dark' ? 'black' : 'green')};
  ${tw`text-4xl`}
`
