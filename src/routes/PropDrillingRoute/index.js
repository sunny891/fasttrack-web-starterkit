import React, { Component } from 'react'
import { ThemedText } from './styledComponents'

class PropDrillingRoute extends Component {
  state = {
    selectedTheme: 'dark'
  }

  onChangeTheme = () => {
    const theme = this.state.selectedTheme
    this.setState({
      selectedTheme: theme === 'dark' ? 'light' : 'dark'
    })
  }
  render() {
    const { selectedTheme } = this.state
    return (
      <div>
        <Header
          selectedTheme={this.state.selectedTheme}
          onChangeTheme={this.onChangeTheme}
        />
        <EmojiList selectedTheme={selectedTheme} />
      </div>
    )
  }
}

const Header = props => {
  return (
    <div>
      <ThemedText selectedTheme={props.selectedTheme}>Header</ThemedText>
      <button
        className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
        onClick={props.onChangeTheme}
      >
        {props.selectedTheme}
      </button>
    </div>
  )
}

class EmojiList extends Component {
  renderEmojiList = () => {
    let emojiData = ['Rounded Face', 'Smile Face', 'Thinking Face']
    return emojiData.map((emotionText, index) => {
      return (
        <EmojiCard
          selectedTheme={this.props.selectedTheme}
          emotionText={emotionText}
          key={index}
        />
      )
    })
  }
  render() {
    return (
      <div className='text-4xl'>
        EmojiList: <div>{this.renderEmojiList()}</div>
      </div>
    )
  }
}

const EmojiCard = props => {
  const { selectedTheme, emotionText } = props
  return (
    <div>
      <ThemedText selectedTheme={props.selectedTheme}>
        {props.emotionText}
      </ThemedText>
    </div>
  )
}

export default PropDrillingRoute
