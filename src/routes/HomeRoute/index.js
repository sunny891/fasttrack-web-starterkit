import React from 'react'
import { Link } from 'react-router-dom'

import ReactLogo from '../../components/common/Icons/ReactLogo'
import {
  SAMPLE_ROUTE_PATH,
  PROP_DRILLING_ROUTE_PATH,
  CONTEXT_ROUTE_PATH,
  INDEX_AS_A_ANTI_PATTERN_ROUTE_PATH,
  REFS_ROUTE_PATH
} from '../../constants/NavigationConstants'

import './index.css'

function HomeRoute() {
  return (
    <div className='app'>
      <header className='bg-indigo-900 app-header'>
        <ReactLogo className='app-logo' />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className='app-link'
          href='https://reactjs.org'
          target='_blank'
          rel='noopener noreferrer'
        >
          Learn React
        </a>
        <Link to={SAMPLE_ROUTE_PATH}>Sample Route</Link>
        <Link to={PROP_DRILLING_ROUTE_PATH}>PropDrilling Route</Link>
        <Link to={CONTEXT_ROUTE_PATH}>Context Route</Link>
        <Link to={INDEX_AS_A_ANTI_PATTERN_ROUTE_PATH}>
          Index as a anti pattern Route
        </Link>
        <Link to={REFS_ROUTE_PATH}>Refs</Link>
      </header>
    </div>
  )
}

export default HomeRoute
