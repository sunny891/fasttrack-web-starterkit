# Concepts

- React Context
  - Prop drilling Problem
  - Multiple providers and Multiple consumers
  - Mobx inject
- Keys in react
  - Index as a key is an anti pattern
- Refs
  - Problem - How to focus a input field on user action?
