import ReactDOM from 'react-dom'
import React from 'react'

class Item extends React.Component {
  render() {
    return (
      <div className='p-6'>
        <label className='block text-gray-700 text-sm font-bold mb-2'>
          id: {this.props.name}
        </label>
        <div>
          <input
            className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight'
            type='text'
          />
        </div>
      </div>
    )
  }
}

class KeyAsAnAnitPatternExample extends React.Component {
  constructor() {
    super()
    this.state = {
      list: [
        { name: 'Foo1', id: 1 },
        { name: 'Foo2', id: 2 }
      ]
    }
  }

  addItem = () => {
    const id = +new Date()
    this.setState({
      list: [{ name: 'Foo' + id, id }, ...this.state.list]
    })
  }

  render() {
    return (
      <div className='p-8'>
        First write something in the inputs. Then hit <em>Add item</em> and see
        what happens…
        <hr />
        <button
          className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
          onClick={this.addItem}
        >
          <b>Add item</b> to the beginning of the list
        </button>
        <div className='flex flex-row'>
          <form className='pl-8 w-1/4'>
            <h3 className='text-red-500 text-4xl italic'>
              Dangerous <code>key=index</code>
            </h3>
            {this.state.list.map((todo, index) => (
              <Item {...todo} />
            ))}
          </form>
          <form className='pl-8 w-1/4'>
            <h3 className='text-green-500 text-4xl italic'>
              Better <code>key=id</code>
            </h3>
            {this.state.list.map(todo => (
              <Item {...todo} key={todo.id} />
            ))}
          </form>
        </div>
      </div>
    )
  }
}

export default KeyAsAnAnitPatternExample
