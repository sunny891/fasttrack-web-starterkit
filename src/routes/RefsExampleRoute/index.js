import React, { Component } from 'react'
import SignInForm from '../../components/SigninForm'

class RefsExampleRoute extends Component {
  signInFormRef = React.createRef()
  onSubmit = () => {
    this.signInFormRef.current.passwordRef.current.focus()
  }
  render() {
    return <SignInForm onSubmit={this.onSubmit} ref={this.signInFormRef} />
  }
}

export default RefsExampleRoute
