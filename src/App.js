import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Provider } from 'mobx-react'

import HomeRoute from './routes/HomeRoute'
import SampleRoute from './routes/SampleRoute'
import PropDrillingRoute from './routes/PropDrillingRoute'
import ContextRoute from './routes/ContextRoute'
import IndexAsAKeyAntiPatternRoute from './routes/IndexAsAKeyAntiPatternRoute'
import RefsExampleRoute from './routes/RefsExampleRoute'

import stores from './stores'

import {
  HOME_ROUTE_PATH,
  SAMPLE_ROUTE_PATH,
  PROP_DRILLING_ROUTE_PATH,
  CONTEXT_ROUTE_PATH,
  INDEX_AS_A_ANTI_PATTERN_ROUTE_PATH,
  REFS_ROUTE_PATH
} from './constants/NavigationConstants'

const App = () => {
  return (
    <Provider {...stores}>
      <Router basename={process.env.PUBLIC_URL}>
        <Switch>
          <Route exact path={SAMPLE_ROUTE_PATH}>
            <SampleRoute />
          </Route>
          <Route path={PROP_DRILLING_ROUTE_PATH}>
            <PropDrillingRoute />
          </Route>
          <Route path={CONTEXT_ROUTE_PATH}>
            <ContextRoute />
          </Route>
          <Route path={INDEX_AS_A_ANTI_PATTERN_ROUTE_PATH}>
            <IndexAsAKeyAntiPatternRoute />
          </Route>
          <Route path={REFS_ROUTE_PATH}>
            <RefsExampleRoute />
          </Route>
          <Route path={HOME_ROUTE_PATH}>
            <HomeRoute />
          </Route>
        </Switch>
      </Router>
    </Provider>
  )
}

export default App
